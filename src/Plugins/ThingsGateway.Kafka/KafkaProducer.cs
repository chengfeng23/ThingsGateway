﻿#region copyright
//------------------------------------------------------------------------------
//  此代码版权声明为全文件覆盖，如有原作者特别声明，会在下方手动补充
//  此代码版权（除特别声明外的代码）归作者本人Diego所有
//  源代码使用协议遵循本仓库的开源协议及附加协议
//  Gitee源代码仓库：https://gitee.com/diego2098/ThingsGateway
//  Github源代码仓库：https://github.com/kimdiego2098/ThingsGateway
//  使用文档：https://diego2098.gitee.io/thingsgateway-docs/
//  QQ群：605534569
//------------------------------------------------------------------------------
#endregion

using Confluent.Kafka;

using Mapster;

using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;

using NewLife.Serialization;

using ThingsGateway.Foundation;
using ThingsGateway.Foundation.Extension;
using ThingsGateway.Web.Foundation;

using TouchSocket.Core;

namespace ThingsGateway.Kafka;

using System.Collections.Concurrent;
using System.Runtime.InteropServices;
public class KafkaProducer : UpLoadBase
{
    private GlobalCollectDeviceData _globalCollectDeviceData;

    private RpcSingletonService _rpcCore;
    private List<CollectVariableRunTime> _uploadVariables = new();
    private CollectDeviceWorker collectDeviceHostService;
    private ConcurrentQueue<DeviceData> _collectDeviceRunTimes = new();
    private ConcurrentQueue<VariableData> _collectVariableRunTimes = new();
    private KafkaProducerProperty driverPropertys = new();
    private EasyLock lockobj = new();
    private IProducer<Null, string> producer;
    private ProducerBuilder<Null, string> producerBuilder;
    private ProducerConfig producerconfig;
    private KafkaProducerVariableProperty variablePropertys = new();

    public KafkaProducer(IServiceScopeFactory scopeFactory) : base(scopeFactory)
    {
    }


    public override DriverPropertyBase DriverPropertys => driverPropertys;


    public override List<CollectVariableRunTime> UploadVariables => _uploadVariables;


    public override VariablePropertyBase VariablePropertys => variablePropertys;
    public override async Task BeforStartAsync(CancellationToken cancellationToken)
    {
        await Task.CompletedTask;
    }
    /// <summary>
    /// 异步执行
    /// </summary>
    /// <param name="cancellationToken"></param>
    /// <returns></returns>
    public override async Task ExecuteAsync(CancellationToken cancellationToken)
    {
        try
        {
            ////变化推送
            var varList = _collectVariableRunTimes.ToListWithDequeue();
            if (varList?.Count != 0)
            {
                //分解List，避免超出mqtt字节大小限制
                var varData = varList.ChunkTrivialBetter(500);
                foreach (var item in varData)
                {
                    try
                    {
                        if (!cancellationToken.IsCancellationRequested)
                        {
                            await KafKaUp(driverPropertys.VariableTopic, item.ToJson(), cancellationToken);
                        }
                        else
                        {
                            break;
                        }
                    }
                    catch (Exception ex)
                    {
                        _logger.LogWarning(ex, ToString());
                    }

                }
                producer.Flush(cancellationToken);

            }
        }
        catch (Exception ex)
        {
            _logger?.LogWarning(ex, ToString());
        }
        try
        {
            ////变化推送
            var devList = _collectDeviceRunTimes.ToListWithDequeue();
            if (devList?.Count != 0)
            {
                //分解List，避免超出mqtt字节大小限制
                var devData = devList.ChunkTrivialBetter(500);
                foreach (var item in devData)
                {
                    try
                    {
                        if (!cancellationToken.IsCancellationRequested)
                        {
                            await KafKaUp(driverPropertys.DeviceTopic, item.ToJson(), cancellationToken);
                        }
                        else
                        {
                            break;
                        }
                    }
                    catch (Exception ex)
                    {
                        _logger.LogWarning(ex, ToString());
                    }
                }
                producer.Flush(cancellationToken);

            }
        }
        catch (Exception ex)
        {
            _logger?.LogWarning(ex, ToString());
        }

        if (driverPropertys.CycleInterval > UploadDeviceThread.CycleInterval + 50)
        {
            try
            {
                await Task.Delay(driverPropertys.CycleInterval - UploadDeviceThread.CycleInterval, cancellationToken);
            }
            catch
            {
            }
        }
        else
        {

        }

    }

    private async Task KafKaUp(string topic, string payLoad, CancellationToken cancellationToken)
    {
        var result = await producer.ProduceAsync(topic, new Message<Null, string> { Value = payLoad }, cancellationToken);
        if (result.Status != PersistenceStatus.Persisted)
        {
            await AddCacheData(topic, payLoad, driverPropertys.CacheMaxCount);
        }
        else
        {
            //连接成功时补发缓存数据
            var cacheData = await GetCacheData();
            foreach (var item in cacheData)
            {
                var result1 = await producer.ProduceAsync(item.Topic, new Message<Null, string> { Value = item.CacheStr }, cancellationToken);

                if (result.Status == PersistenceStatus.Persisted)
                {
                    logMessage.Trace("报文-" + $"主题：{item.Topic}{Environment.NewLine}负载：{item.CacheStr}");

                    await DeleteCacheData(item.Id);
                }
            }
            logMessage.Trace("报文-" + $"主题：{topic}{Environment.NewLine}负载：{payLoad}");

        }
    }

    public override OperResult IsConnected()
    {
        return producer == null ? new("初始化失败") : OperResult.CreateSuccessResult();
    }

    protected override void Dispose(bool disposing)
    {
        producer.Dispose();
        _uploadVariables = null;
        _collectDeviceRunTimes.Clear();
        _collectVariableRunTimes.Clear();
        _collectDeviceRunTimes = null;
        _collectVariableRunTimes = null;
        base.Dispose(disposing);
    }

    /// <summary>
    /// 初始化
    /// </summary>
    /// <param name="device"></param>
    protected override void Init(UploadDeviceRunTime device)
    {
        #region Kafka 生产者
        //1、生产者配置
        producerconfig = new ProducerConfig
        {
            BootstrapServers = driverPropertys.BootStrapServers,
            ClientId = driverPropertys.ClientId,
        };
        //2、创建生产者
        producerBuilder = new ProducerBuilder<Null, string>(producerconfig);
        //3、错误日志监视
        producerBuilder.SetErrorHandler((p, msg) =>
        {
            _logger.LogWarning($"Producer_Erro信息：Code：{msg.Code}；Reason：{msg.Reason}；IsError：{msg.IsError}");
        });
        //kafka
        try
        {
            producer = producerBuilder.Build();
        }
        catch (DllNotFoundException)
        {
            if (!Library.IsLoaded)
            {
                string osStr = "win-";
                if (RuntimeInformation.IsOSPlatform(OSPlatform.Windows))
                    osStr = "win-";
                else if (RuntimeInformation.IsOSPlatform(OSPlatform.Linux))
                    osStr = "linux-";
                else
                    osStr = "osx-";
                osStr += RuntimeInformation.ProcessArchitecture.ToString().ToLower();
                var pathToLibrd = System.IO.Path.Combine(AppContext.BaseDirectory, "Plugins", "ThingsGateway.Kafka", "runtimes", osStr, "native", "librdkafka");
                Library.Load(pathToLibrd);
            }
            producer = producerBuilder.Build();
        }
        #endregion

        var serviceScope = _scopeFactory.CreateScope();
        _globalCollectDeviceData = serviceScope.ServiceProvider.GetService<GlobalCollectDeviceData>();
        _rpcCore = serviceScope.ServiceProvider.GetService<RpcSingletonService>();
        collectDeviceHostService = serviceScope.GetBackgroundService<CollectDeviceWorker>();

        var tags = _globalCollectDeviceData.CollectVariables.Where(a => a.VariablePropertys.ContainsKey(device.Id))
           .Where(b => GetPropertyValue(b, nameof(variablePropertys.Enable)).GetBoolValue()).ToList();

        _uploadVariables = tags;

        _globalCollectDeviceData.CollectDevices.Where(a => _uploadVariables.Select(b => b.DeviceId).Contains(a.Id)).ForEach(a =>
        {
            a.DeviceStatusCahnge += DeviceStatusCahnge;
        });
        _uploadVariables.ForEach(a =>
        {
            a.VariableValueChange += VariableValueChange;
        });


    }

    private void DeviceStatusCahnge(CollectDeviceRunTime collectDeviceRunTime)
    {
        _collectDeviceRunTimes.Enqueue(collectDeviceRunTime.Adapt<DeviceData>());
    }

    private void handler(DeliveryReport<Null, string> r)
    {
        if (!r.Error.IsError)
        {
            _logger.LogTrace($"Delivered message to {r.TopicPartitionOffset}");
        }
        else
        {
            _logger.LogWarning($"Delivery Error: {r.Error.Reason}");
        }
    }
    private void VariableValueChange(CollectVariableRunTime collectVariableRunTime)
    {
        _collectVariableRunTimes.Enqueue(collectVariableRunTime.Adapt<VariableData>());
    }
}